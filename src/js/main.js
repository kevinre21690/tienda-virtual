/*=========SCROLL REVEAL ANIMATION===========*/
let sr = ScrollReveal({
    origin: 'top',
    distance: '30px',
    duration: 2000,
    reset: true
});

sr.reveal(`.section_banners_container, .boton_atras_banners,
            .banners_content, .boton_siguiente_banners,
            .titulo_categorias, .container_cuadros_servicios,
            .productos_content_principales_slider, .cuadro_texto_nosotros, 
            .scroll_titulo_valores, .scroll_cuadros_nosotros,
            .cuadro_texto_contactos, .formulario_contacto,
            .container_cuadros_footer`, {
    interval: 200
});


/*=======SCROLL TOP==========*/
function scrollTop() {
    const scrollTop = document.getElementById('scroll-top');
    if (this.scrollY >= 560) scrollTop.classList.add('show-scroll');
    else scrollTop.classList.remove('show-scroll')
}
window.addEventListener('scroll', scrollTop)


/*=======SHOW MENU=====*/

jQuery('document').ready(function($) {
    var btn = $('.bx-menu'),
        show_menu = $('.menu_content');

    btn.click(function() {
        if (show_menu.hasClass('show')) {
            show_menu.removeClass('show');
        } else {
            show_menu.addClass('show');


        }

    });

});


/*=======SHOW PANEL USER=====*/

jQuery('document').ready(function($) {
    var btn = $('.btn_iniciar_session'),
        show_panel_user = $('.panel_user');

    btn.click(function() {
        if (show_panel_user.hasClass('show')) {
            show_panel_user.removeClass('show');
        } else {
            show_panel_user.addClass('show');


        }

    });

});


/*=======SHOW PANEL SESSION USER=====*/

jQuery('document').ready(function($) {
    var btn = $('.btn_iniciar_session'),
        show_panel_user = $('.panel_user_session');

    btn.click(function() {
        if (show_panel_user.hasClass('show')) {
            show_panel_user.removeClass('show');
        } else {
            show_panel_user.addClass('show');


        }

    });

});


/*==========SHOW PASSWD PANEL USER=========*/

function hideOrShowPassword(){
    var input_contraseña
    var icon_password;
  console.log("Entra")
    input_contraseña=document.getElementById("input_contraseña");
    icon_password=document.getElementById("panel_user_icon_password");
    eye=document.getElementById("eye");

    if(input_contraseña.type == "password") {
        input_contraseña.type = "text"
        eye.className = "fa-solid fa-eye panel_user_icon_password"
    } else {
        eye.className = "fa-solid fa-eye-slash panel_user_icon_password"
        input_contraseña.type = "password"
    }

  }


  /*==========SHOW PASSWD REGISTRO=========*/

function hideOrShowPasswordResgitro(){
    var crear_contraseña
    var icon_password;
  console.log("Entra")
    crear_contraseña=document.getElementById("crear_contraseña");
    icon_password=document.getElementById("registro_icon_password");
    eye_registro_1=document.getElementById("eye_registro_1");

    if(crear_contraseña.type == "password") {
        crear_contraseña.type = "text";
        eye_registro_1.className = "fa-solid fa-eye registro_icon_password"
    } else {
        crear_contraseña.type = "password";
        eye_registro_1.className = "fa-solid fa-eye-slash registro_icon_password"
    }

  }


    /*==========SHOW PASSWD REPETIR REGISTRO=========*/

function hideOrShowPasswordResgitro2(){
    var repetir_crear_contraseña
    var icon_password;
  console.log("Entra")
    repetir_crear_contraseña=document.getElementById("repetir_crear_contraseña");
    icon_password=document.getElementById("registro_repetir_icon_password");
    eye_registro_2=document.getElementById("eye_registro_2");

    if(repetir_crear_contraseña.type == "password") {
        repetir_crear_contraseña.type = "text";
        eye_registro_2.className = "fa-solid fa-eye registro_repetir_icon_password"
    } else {
        repetir_crear_contraseña.type = "password";
        eye_registro_2.className = "fa-solid fa-eye-slash registro_repetir_icon_password"
    }

  }


  /*===========VALIDACIONES DEL REGISTRO================*/

const formulario = document.getElementById('container_formulario_registro');
const inputs = document.querySelectorAll('#container_formulario_registro input');

const expresiones = {
	usuario: /^[a-zA-Z0-9\_\-\.]{4,16}$/, // Letras, numeros, guion y guion_bajo
	nombre: /^[a-zA-Z\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    telefono: /^\d{7,15}$/, // 7 a 14 numeros.
    contraseña1: /^[a-zA-Z0-9\_\.\+\*\#\$\%\&\!\@]{4,12}$/, // 4 a 12 digitos.
}

const campos = {
	usuario: false,
	nombre: false,
    correo: false,
	telefono: false,
	contraseña1: false,
}

const validarFormulario = (e) => {
	switch (e.target.name) {
		case "usuario":
			validarCampo(expresiones.usuario, e.target, 'usuario');
		break;
		case "nombre":
			validarCampo(expresiones.nombre, e.target, 'nombre');
		break;
        case "correo":
			validarCampo(expresiones.correo, e.target, 'correo');
		break;
		case "telefono":
			validarCampo(expresiones.telefono, e.target, 'telefono');
		break;
		case "contraseña1":
			validarCampo(expresiones.password, e.target, 'password');
			validarPassword2();
		break;
		case "contraseña2":
			validarPassword2();
		break;
	}
}

const validarCampo = (expresion, input, campo) => {
	if(expresion.test(input.value)){
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-check-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-times-circle');
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.remove('formulario__input-error-activo');
		campos[campo] = true;
	} else {
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-times-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-check-circle');
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.add('formulario__input-error-activo');
		campos[campo] = false;
	}
}

const validarPassword2 = () => {
	const inputPassword1 = document.getElementById('contraseña1');
	const inputPassword2 = document.getElementById('contraseña2');

	if(inputPassword1.value !== inputPassword2.value){
		document.getElementById(`grupo__password2`).classList.add('formulario__grupo-incorrecto');
		document.getElementById(`grupo__password2`).classList.remove('formulario__grupo-correcto');
		document.querySelector(`#grupo__password2 i`).classList.add('fa-times-circle');
		document.querySelector(`#grupo__password2 i`).classList.remove('fa-check-circle');
		document.querySelector(`#grupo__password2 .formulario__input-error`).classList.add('formulario__input-error-activo');
		campos['password'] = false;
	} else {
		document.getElementById(`grupo__password2`).classList.remove('formulario__grupo-incorrecto');
		document.getElementById(`grupo__password2`).classList.add('formulario__grupo-correcto');
		document.querySelector(`#grupo__password2 i`).classList.remove('fa-times-circle');
		document.querySelector(`#grupo__password2 i`).classList.add('fa-check-circle');
		document.querySelector(`#grupo__password2 .formulario__input-error`).classList.remove('formulario__input-error-activo');
		campos['password'] = true;
	}
}

inputs.forEach((input) => {
	input.addEventListener('keyup', validarFormulario);
	input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('submit', (e) => {
	e.preventDefault();

	const terminos = document.getElementById('terminos');
	if(campos.usuario && campos.nombre && campos.password && campos.correo && campos.telefono && terminos.checked ){
		formulario.reset();

		document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');
		setTimeout(() => {
			document.getElementById('formulario__mensaje-exito').classList.remove('formulario__mensaje-exito-activo');
		}, 5000);

		document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
			icono.classList.remove('formulario__grupo-correcto');
		});
	} else {
		document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
	}
});