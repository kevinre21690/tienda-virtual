<?php 
    include("./sql.php");
    require('./layout/header.php');
?>

    <main>
    <section class="section_registro">
            <form class="container_formulario_registro" id="container_formulario_registro" action="./conexion_registro.php" method="POST">
                <div class="content_formulario_registro">
                    <div class="registro_lado_izquierdo">
                        <div class="campo_usuario" id="grupo__usuario">
                            <input class="usuario" type="text" name= "usuario" require="" placeholder="ID de usuario">
                            <p class="formulario__input-error">aA zZ/4 a 16 caracteres/.-_</p>
                        </div>
                        <div class="campo_nombre" id="grupo__nombre">
                            <input class="nombre_de_usuario" name= "nombre" type="text" required="" placeholder="Nombre completo">
                            <p class="formulario__input-error">4 a 16 caracteres</P>
                        </div>
                        <div class="campo_correo" id="grupo__correo">
                            <input class="correo_electronico" name= "correo" type="text" required="" placeholder="Correo electronico">
                            <p class="formulario__input-error">correo electronico no valido</P>
                        </div>
                        <div class="campo_telefono" id="grupo__telefono">
                        <input class="telefono" type="number" name= "telefono" required="" placeholder="Telefono/Celular">
                        <p class="formulario__input-error">Maximo 12 digitos</P>
                        </div>
                    </div>
                    <div class="registro_lado_derecho">
                        <div class="campo_tdocumento" id="grupo__tdocumento">
                        <select class="tipo_documento" name="tipo_documento">
                            <option disabled selected>Tipo de documento</option>
                            <option>Cedula de ciudadanía</option>
                            <option>Cedula de extranjería</option>
                            <option>Pasaporte</option>
                        </select>
                        <p class="formulario__input-error">4 a 16 caracteres</P>
                        </div>
                        <div class="campo_ndocumento" id="grupo__ndocumento">
                        <input class="numero_de_documento" name= "documento" type="number" require="" placeholder="Número de documento">
                        <p class="formulario__input-error">Maximo 15 digitos</P>
                        </div>
                        <div class="campo_password1" id="grupo__password1">
                        <input class="crear_contraseña" id="crear_contraseña" name= "contraseña1" type="password" required="" placeholder="Crear contraseña">
                        <i id="eye_registro_1" class="fa-solid fa-eye-slash registro_icon_password" id="registro_icon_password" onClick="hideOrShowPasswordResgitro()"></i>
                        <p class="formulario__input-error">Maximo 15 digitos</P>
                        </div>
                        <div class="campo_password2" id="grupo__password2">
                        <input class="repetir_crear_contraseña" id="repetir_crear_contraseña" name= "contraseña2" type="password" required="" placeholder="Repetir contraseña">
                        <i id="eye_registro_2" class="fa-solid fa-eye-slash registro_repetir_icon_password" id="registro_repetir_icon_password" onClick="hideOrShowPasswordResgitro2()"></i>
                        <p class="formulario__input-error">Maximo 15 digitos</P>
                        </div>
                    </div>
                </div>
                <div class="g-recaptcha" data-sitekey="6LdAFTweAAAAAF3Gz0HJWEOsICNF2LlWVAokiUGj" data-callback="correctCaptcha"></div>
                    <?php
                        if (isset($_POST['submitForm'])) {
                            $captcha_response = true;
                            $recaptcha = $_POST['g-recaptcha-response'];
                        
                            $url = 'https://www.google.com/recaptcha/api/siteverify';
                            $data = array(
                                'secret' => '6LdAFTweAAAAAHqc10TbNOQnKn0ZqhonTCBqLGfC',
                                'response' => $recaptcha
                            );
                            $options = array(
                                'http' => array (
                                    'method' => 'POST',
                                    'content' => http_build_query($data)
                                )
                            );
                            $context  = stream_context_create($options);
                            $verify = file_get_contents($url, false, $context);
                            $captcha_success = json_decode($verify);
                            $captcha_response = $captcha_success->success;
                            if ($captcha_response) {
                                echo '<p class="alert alert-success">Procesar datos...</p>';
                            } else {
                                echo '<p class="alert alert-danger">Debes indicar que no eres un robot.';
                            }
                        }
                    ?>
                </div>    
                <div class="content_boton_registrste">
                    <button class="registrate" input type="submit" name="registrar">Registrate</button>
                </div>
            </form>
        </section>
    </main>
    <script src="https://www.google.com/recaptcha/api.js"></script>

<?php require('./layout/footer.php');?>

