

    
    <header class="header" id="header">
        <div class="barra_nav" id="barra_nav">
            <div class="logo_content">
                <a href="./index.php" class="logo" id="logo">Tienda virtual</a>
            </div>
            <div class="menu_slider">
                <nav class="menu_content" id="menu_content">
                    <ul class="menu_items">
                        <li class="menu_item">
                            <a href="">
                                <div class="icon_content">
                                    <i class="fa-solid fa-bowl-food"></i>
                                </div>
                                <div class="p_content">
                                    <p>Alimentos y despensa</p>
                                </div>
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="">
                                <div class="icon_content">
                                    <i class="fa-solid fa-house"></i>
                                </div>
                                <div class="p_content">
                                    <p>Hogar</p>
                                </div>
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="">
                                <div class="icon_content">
                                    <i class="fa-solid fa-bowl-food"></i>
                                </div>
                                <div class="p_content">
                                    <p>Aseo</p>
                                </div>
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="">
                                <div class="icon_content">
                                    <i class="fa-solid fa-bowl-food"></i>
                                </div>
                                <div class="p_content">
                                    <p>Bebe</p>
                                </div>
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="">
                                <div class="icon_content">
                                    <i class="fa-solid fa-bowl-food"></i>
                                </div>
                                <div class="p_content">
                                    <p>Bebidas</p>
                                </div>
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="">
                                <div class="icon_content">
                                    <i class="fa-solid fa-bowl-food"></i>
                                </div>
                                <div class="p_content">
                                    <p>Congelados</p>
                                </div>
                            </a>
                        </li>
                        <li class="menu_item">
                            <a href="">
                                <div class="icon_content">
                                    <i class="fa-solid fa-bowl-food"></i>
                                </div>
                                <div class="p_content">
                                    <p>Extraordinarios</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>    
            <div class="search_content">
                <div class="search_section">
                    <input class="search" type="search" placeholder="Buscar" name="search" id="search">
                </div>
            </div>
            <div class="user_content">
                <div class="btns_content_header">
                    <div class="btn_login_content">
                        <button class="btn_login_header"><a>Login</a></button>
                    </div>
                    <div class="btn_singUp_content">
                        <button class="btn_singUp_header"><a href="layout/Form_registro.php">Sing up</a></button>
                    </div>
                </div>
            </div>
            <!-- <div class="carrito_container">
                <div class="carrito_content"></div>
                <i class='bx bx-cart'></i>
                <div class="panel_de_compras"></div>
            </div> -->
        </div>
    </header>
