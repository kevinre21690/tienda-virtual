    <main>  
        <div class="main_board">
            <section class="container_slide_banner">
                <div class="content_slide_banner">
                    <input class="slide-open" type="radio" id="slide-1" 
                        name="slide" aria-hidden="true" hidden="" checked="checked">
                    <div class="slide-item">
                        <img src="src/imagenes/banner-1.jpg">
                    </div>
                    <input class="slide-open" type="radio" id="slide-2" 
                        name="slide" aria-hidden="true" hidden="">
                    <div class="slide-item">
                        <img src="src/imagenes/banner-2.jpg">
                    </div>
                    <input class="slide-open" type="radio" id="slide-3" 
                        name="slide" aria-hidden="true" hidden="">
                    <div class="slide-item">
                        <img src="src/imagenes/banner-3.jpg">
                    </div>
                    <label for="slide-3" class="slide-control prev control-1">‹</label>
                    <label for="slide-2" class="slide-control next control-1">›</label>
                    <label for="slide-1" class="slide-control prev control-2">‹</label>
                    <label for="slide-3" class="slide-control next control-2">›</label>
                    <label for="slide-2" class="slide-control prev control-3">‹</label>
                    <label for="slide-1" class="slide-control next control-3">›</label>
                    <ol class="slide-indicador">
                        <li>
                            <label for="slide-1" class="slide-circulo">•</label>
                        </li>
                        <li>
                            <label for="slide-2" class="slide-circulo">•</label>
                        </li>
                        <li>
                            <label for="slide-3" class="slide-circulo">•</label>
                        </li>
                    </ol>
                </div>
            </section>
            
            <section class="section_products_alimentos_y_despensa">
                <div class="container_productos_alimentos_y_despensa">
                    <div class="titulo_categorias">Alimentos y despensa</div>
                    <div class="productos_content_principales_slider">
                        <div class="cuadros_productos_alimentos_y_despensa">
                            <img class="Imagen_producto" src="src/imagenes/salchichon_rica.png" alt="">
                            <p  class="valor_producto">$12.000</p>
                            <h3 class="nombre_producto">Salchichon Rica</h3>
                            <p  class="descripcion_producto">284 g (g a $38,7)</p>
                            <div class="icon_agregar">
                                <i class='bx bx-plus' ></i>
                            </div>
                        </div>
                        <div class="cuadros_productos_alimentos_y_despensa">
                            <img class="Imagen_producto" src="src/imagenes/frijol_chino.png" alt="">
                            <p  class="valor_producto">$12.000</p>
                            <h3 class="nombre_producto">Frijol Chino</h3>
                            <p  class="descripcion_producto">284 g (g a $38,7)</p>
                            <div class="icon_agregar">
                                <i class='bx bx-plus' ></i>
                            </div>
                        </div>
                        <div class="cuadros_productos_alimentos_y_despensa">
                            <img class="Imagen_producto" src="src/imagenes/mazamorra.png" alt="">
                            <p  class="valor_producto">$12.000</p>
                            <h3 class="nombre_producto">Mazamorra</h3>
                            <p  class="descripcion_producto">284 g (g a $38,7)</p>
                            <div class="icon_agregar">
                                <i class='bx bx-plus' ></i>
                            </div>
                        </div>
                </div>
            </section>

            <section class="section_productos_hogar">
                <div class="container_productos_hogar">
                    <div class="titulo_categorias">Hogar</div>
                    <div class="productos_content_principales_slider">
                        <div class="cuadros_productos_hogar">
                            <img class="Imagen_producto" src="src/imagenes/salchichon_rica.png" alt="">
                            <p  class="valor_producto">$12.000</p>
                            <h3 class="nombre_producto">Salchichon Rica</h3>
                            <p  class="descripcion_producto">284 g (g a $38,7)</p>
                            <div class="icon_agregar">
                                <i class='bx bx-plus' ></i>
                            </div>
                        </div>
                        <div class="cuadros_productos_alimentos_y_despensa">
                            <img class="Imagen_producto" src="src/imagenes/frijol_chino.png" alt="">
                            <p  class="valor_producto">$12.000</p>
                            <h3 class="nombre_producto">Frijol Chino</h3>
                            <p  class="descripcion_producto">284 g (g a $38,7)</p>
                            <div class="icon_agregar">
                                <i class='bx bx-plus' ></i>
                            </div>
                        </div>
                        <div class="cuadros_productos_alimentos_y_despensa">
                            <img class="Imagen_producto" src="src/imagenes/mazamorra.png" alt="">
                            <p  class="valor_producto">$12.000</p>
                            <h3 class="nombre_producto">Mazamorra</h3>
                            <p  class="descripcion_producto">284 g (g a $38,7)</p>
                            <div class="icon_agregar">
                                <i class='bx bx-plus' ></i>
                            </div>
                        </div>
                    </div>
                </div>    
            </section>

            <section class="section_products_alimentos_y_despensa">
                <div class="container_productos_alimentos_y_despensa">
                    <div class="titulo_categorias">Alimentos y despensa</div>
                    <div class="productos_content_principales_slider">
                        <div class="cuadros_productos_alimentos_y_despensa">
                            <img class="Imagen_producto" src="src/imagenes/salchichon_rica.png" alt="">
                            <p  class="valor_producto">$12.000</p>
                            <h3 class="nombre_producto">Salchichon Rica</h3>
                            <p  class="descripcion_producto">284 g (g a $38,7)</p>
                            <div class="icon_agregar">
                                <i class='bx bx-plus' ></i>
                            </div>
                        </div>
                        <div class="cuadros_productos_alimentos_y_despensa">
                            <img class="Imagen_producto" src="src/imagenes/frijol_chino.png" alt="">
                            <p  class="valor_producto">$12.000</p>
                            <h3 class="nombre_producto">Frijol Chino</h3>
                            <p  class="descripcion_producto">284 g (g a $38,7)</p>
                            <div class="icon_agregar">
                                <i class='bx bx-plus' ></i>
                            </div>
                        </div>
                        <div class="cuadros_productos_alimentos_y_despensa">
                            <img class="Imagen_producto" src="src/imagenes/mazamorra.png" alt="">
                            <p  class="valor_producto">$12.000</p>
                            <h3 class="nombre_producto">Mazamorra</h3>
                            <p  class="descripcion_producto">284 g (g a $38,7)</p>
                            <div class="icon_agregar">
                                <i class='bx bx-plus' ></i>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>