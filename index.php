<!DOCTYPE html=es>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <script src="https://unpkg.com/scrollreveal"></script>
        <a href="#" class="scrolltop" id="scroll-top">
        <script src="./src/js/jquery-3.6.0.min.js"></script>
            <ion-icon class="scrolltop__ico" name="chevron-up-circle-outline"></ion-icon>
        </a>
        <link rel="stylesheet" href="./src/css/estilo.css">
        <link rel="stylesheet" href="./src/css/header.css">
        <link rel="stylesheet" href="./src/css/main.css">
        <!-- <script src="src/js/main.js"></script> -->
        <title>Tienda virtual</title>
    </head>

    <body>

        <?php
            require('./layout/header.php');
        ?>
        <?php
            require('./layout/main.php');
        ?>
        <!-- <?php
            require('./layout/footer.php');
        ?> -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/js/all.min.js" integrity="sha512-naukR7I+Nk6gp7p5TMA4ycgfxaZBJ7MO5iC3Fp6ySQyKFHOGfpkSZkYVWV5R7u7cfAicxanwYQ5D1e17EfJcMA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        
    </body>

</html>